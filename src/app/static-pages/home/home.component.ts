import { Component, OnInit } from '@angular/core';
import { ItunesService } from '../../services/itunes.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  songs: any[];

  constructor(
  	private itunesService: ItunesService
  ) { }

  ngOnInit() {
    console.log('Now in home component!');
  	this.loadSongs();
  }

  loadSongs() {
    this.itunesService.getItunesSongs().subscribe(result => {
      this.songs = result;
      console.log(this.songs);
    });
  } 

}
