import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleServicesComponent } from './sample-services.component';

describe('SampleServicesComponent', () => {
  let component: SampleServicesComponent;
  let fixture: ComponentFixture<SampleServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
