import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sample-services',
  templateUrl: './sample-services.component.html',
  styleUrls: ['./sample-services.component.css']
})
export class SampleServicesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  	console.log('Inside Service Info component');
  }

}
