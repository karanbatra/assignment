import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ItunesService } from '../../services/itunes.service';

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.css']
})
export class AlbumListComponent implements OnInit {
  albums : any[];
  @Output() albumInfo = new EventEmitter<object>();
  constructor(
	private itunesService: ItunesService
  ) { }

  ngOnInit() {
  	console.log('Now in Album List component!');
  	this.loadItunesAlbums();
  }

  loadItunesAlbums(){
  	this.itunesService.getItunesAlbums().subscribe(result => {
      this.albums = result;
      console.log(this.albums);
    });
  }

  passAlbumInfoToParent(data) {
    this.albumInfo.emit(data);
  }



}
