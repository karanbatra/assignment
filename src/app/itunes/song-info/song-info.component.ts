import { Component, OnInit } from '@angular/core';
import { ItunesService } from '../../services/itunes.service';

@Component({
  selector: 'app-song-info',
  templateUrl: './song-info.component.html',
  styleUrls: ['./song-info.component.css']
})
export class SongInfoComponent implements OnInit {
  song;
  constructor(
  	private itunesService: ItunesService
  ) { }

  ngOnInit() {
    this.itunesService.currentSong.subscribe(result => {
      console.log('In song info component ', result);      
      if(Object.entries(result).length === 0 && result.constructor === Object){
        this.song = JSON.parse(window.localStorage.getItem('dataSource'));
      }else{
        this.song = result;
        window.localStorage.setItem('dataSource', JSON.stringify(this.song));   
      }
      
    })
  }

}
