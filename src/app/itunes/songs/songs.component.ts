import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { ItunesService } from '../../services/itunes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongsComponent implements OnInit {
	
  @Input() songs: any;
  constructor(
  	 private itunesService: ItunesService,
     private router: Router
  ) { }

  ngOnInit() {
    console.log('Song Component');
   // console.log(this.songs);
  }

  info(song) {
  console.log('Passing Song Info');
    this.itunesService.shareSong(song);
    this.router.navigateByUrl('/song-info');
  }

}
