import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {


  albumHeadingInfo: any;
  constructor() { }

  ngOnInit() {
  }

  fetchAlbumInfo($event) {
    this.albumHeadingInfo = $event;
    console.log($event);
  }

}
