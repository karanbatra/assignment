import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MainComponent } from './layouts/main/main.component';
import { HomeComponent } from './static-pages/home/home.component';
import { AboutComponent } from './static-pages/about/about.component';
import { SampleServicesComponent } from './static-pages/sample-services/sample-services.component';
import { AlbumsComponent } from './itunes/albums/albums.component';
import { AlbumListComponent } from './itunes/album-list/album-list.component';
import { SongInfoComponent } from './itunes/song-info/song-info.component';

import { SongsComponent } from './itunes/songs/songs.component';
import { RegistrationComponent } from './forms/registration/registration.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HomeComponent,
    AboutComponent,
    SampleServicesComponent,
    AlbumsComponent,
    AlbumListComponent,
    SongInfoComponent, 
    SongsComponent, RegistrationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
