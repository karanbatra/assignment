import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MainComponent } from  './layouts/main/main.component';
import { HomeComponent } from  './static-pages/home/home.component';
import { AboutComponent } from  './static-pages/about/about.component';
import { SongInfoComponent } from  './itunes/song-info/song-info.component';
import { AlbumsComponent } from  './itunes/albums/albums.component';

const routes: Routes = [
  {
    path:  '',
    component:  MainComponent,
    children: [
      {
        path:  '',
        component:  HomeComponent
      },
      {
        path:  'about-us',
        component: AboutComponent
      },
      {
        path:  'albums',
        component: AlbumsComponent
      },
            {
        path:  'song-info',
        component: SongInfoComponent
      }, 
     ] 
  }];

@NgModule({
  declarations: [],
    imports: [CommonModule, RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
